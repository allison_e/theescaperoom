"The Escape Room" by "Allison Edwards"


Book One - The Setting


Part Initialisation


Chapter Extensions

Include Erotic Storytelling by Fictitious Frode.
Include Basic Screen Effects by Emily Short.
Include Glulx Text Effects by Emily Short.


Chapter Text Styles

Table of User Styles (continued)
style name    	 color  	italic	 relative size
all-styles		"#000000"	false		--
header-style	"#000000"	false		1
special-style-1	"#cc0000"	false		0
special-style-2	"#0000cc"	true		0


Chapter VM and standard rule tweaks

After asking which do you mean: say "(Just type a word or two to give me more information, or select by number 1,2 etc.)"

Part The Basics

Chapter Tracking the unfortunates

Every person has a number called SAN. SAN is usually 100.
Every person has a number called Dignity. Dignity is usually 100.
Every person has a number called ArousalPc. ArousalPc is usually 0.
A person can be standing or kneeling. A person is usually standing .

Chapter Status bar

Table of Status Lines
left	central	right
"[Posture of the player] in [Location]"		"DGN: [dignity of the player] SAN:[SAN of the player]"

Rule for constructing the status line:
	fill status bar with Table of Status Lines;
	[say default letters;]
	rule succeeds.


Chapter Posture

kneeling is an action applying to nothing.
Understand "kneel" as kneeling.
standing is an action applying to nothing.


to decide what text is the posture of ( the actor - a person):
	if the actor is kneeling:
		 decide on "kneeling";
	otherwise:
		 decide on "standing"

Understand "stand" as standing.
Understand "stand up" as standing.
Instead of exiting, try standing.

check an actor kneeling:
	if the actor is kneeling:
		say "But you are already on the floor" instead.


carry out an actor kneeling:
	now the actor is kneeling.


check an actor standing:
	if the actor is standing:
		say "But you are already standing." instead;

carry out an actor standing:
	now the actor is standing.


report an actor kneeling:
	if  actor is the player:
		say "You kneel down.";
	else:
		if the location of the player is the location of the actor:
			say "[actor] kneels down."


report an actor standing:
	if  actor is the player:
		say "You stand up.";
	else:
		if the location of the player is the location of the actor:
			say "[actor] stands up."


before doing something to the noun while the player is kneeling:
	if the noun is contained by a wardrobe:
		say "You can't reach that while kneeling.[line break]";
		stop the action;
	if a dressing table supports the noun or the noun is contained by something on a dressing table:
		say "You can't reach that while kneeling.[line break]";
		stop the action.


Chapter Basic Kinds

a non-player-character is a kind of person. The printed plural name of a non-player-character is "friends".
not-a-person is a person.

A Wardrobe is a kind of openable closed container. A wardrobe is fixed in place.
A dressing table is a kind of supporter. A dressing table is fixed in place.
A wall-dildo is a kind of sex toy . A wall-dildo is fixed in place.
a wall-dildo has a person called the user. the user is usually not-a-person.
A sex toy can be lickable or unlickable. A sex toy is usually lickable. 
A screen is a kind of thing. A screen is fixed in place.
A screen has some text called the displayed message. The displayed message is usually "Insert analyser into mouth for tonsil examination and further instructions". The description is "a computer like display which currently reads '[displayed message]'".


A garment has some text called colour.

cup size is kind of value. The cup sizes are Flat, AA, B, C, D,  E.
breasts has a cup size called size. The size of breasts is normally Flat.

A bra has a cup size called size.


Chapter The body

[[We need dynamic genitalia so we can do TG easily ]]
Genitalia is a kind of body part.
The specification of genitalia is "Represents the vagina or penis of a person. It is usually indecent and both rubbable and lickable. It can be switched dynamically for TG".
Genitalia is usually penetrating.
Genitalia is usually lickable.
Genitalia is usually rubbable.
The description of a genitalia is "[if penetrating]hard and firm[else]wet and welcoming[end if]".
The printed name of genitalia is "[if penetrating][psize] penis[else]vagina[end if]".
the plural of genitalia is genitalia.
penis size is a kind of value. The penis sizes are Inny, Tiny, Small, Large, Extra Large.
Genitalia has a penis size called psize.


some breasts are a part of every person.
the short description of breasts is "[if size of the breasts of the noun is flat]manly pectoral muscles[otherwise if size of the breasts of the noun is less than  B]fine if somewhat small womanly breasts[otherwise if size of the breasts of the noun is less than D]fine womanly breasts[otherwise]well endowed womanly décolletage[end if]".
a head is part of every person.
some hair is part of every person.
hair length is a kind of value. the hair lengths are bald, buzz cut, shoulder length and waist length.
hair style is a kind of value. the hair styles are not styled, pigtails, ringlets, ponytail, curls  and bun.
a mouth is part of every person.
thighs are a part of every person.
legs are a part of every person.
feet are a part of every person.
arms are a part of every person.
neck is part of every person.
hands are a part of every person.
a waist is part of every person.
a backside is part of every person.
an ass is part of every person.
genitalia are a part of every person. the psize of genitalia is usually large.
the short description of genitalia is "[if psize of the genitalia of the noun is less than tiny]tight looking vagina[otherwise if psize of the genitalia of the noun is less than Large]fine if somewhat small John Thomas[otherwise if psize of the genitalia of the noun is less than Extra Large]good sized cock[otherwise]dick large enough for it's own credit in a porn movie.[end if]".


Description notability for a body part (called x): If x can be seen, distinct.
Description notability for a garment (called x): If x can be seen, distinct.


to decide which breasts is breasts of (x - person):
	repeat with possible running through the breasts which are a part of x:
		decide on possible;
	decide on nothing.

to decide which genitalia is genitalia of (x - person):
	repeat with possible running through the genitalia which are a part of x:
		decide on possible;
	decide on nothing.


Chapter Behaviour and stats

to reduce (the victim - a person) sanity by (amount - a number):
	now the SAN of the victim is the SAN of the victim - amount;
	if the SAN of the victim is less than 0:
		now the SAN of the victim is 0.

to reduce (the victim - a person) dignity by (amount - a number):
	now the dignity of the victim is the dignity of the victim - amount;
	if the Dignity of the victim is less than 0:
		now the Dignity of the victim is 0.



Wearing a garment is feminising behaviour.
fucking something orificial with something is degrading behaviour.

report going somewhere while the player is kneeling:
	say "You crawl [noun] on your hands and knees, like some sort of animal.";
	let dgn_loss be a random number from 1 to 3;
	reduce the player dignity by dgn_loss.

Licking a wall-dildo is degrading behaviour.

After wearing a garment for the first time:
	say "You felt a bit apprehensive about wearing women's clothing,  they are just clothes after all and
	it's not as if there is a lot of choice. Despite this you still felt like it was the first step on a slippery slope.";
	continue the action.

After feminising behaviour:
	let actor be the player;
	let san_loss be a random number from 1 to 4;[Ie. 1d4 SAN loss]
	reduce actor sanity by san_loss;
	let sanity_test be a random number from 0 to 99;
	if sanity_test is greater than the SAN of actor:
	     [ Failed SAN test TODO - Invoke some sort of penalty (assign a fetish?)] 
		say "[Current action], seemed perfectly normal and for some reason that worries you.";
	else:
		say "You feel that by [current action] you've moved further down that slippery slope to.. well what exactly.";
	continue the action.

after degrading behaviour:
	let actor be the player;
	let dgn_loss be a random number from 1 to 6;
	reduce the actor dignity by dgn_loss;
	Say "You realise  [current action] has stripped you of some dignity.";
	continue the action.


Chapter The Group

npc1 is a non-player-character.
npc2 is a non-player-character.
npc3 is a non-player-character.

understand the printed name property as describing a non-player-character.


Table of Male Names
Name
"Bill"
"Robert"
"James"
"Paul"
"John"
"Tim"
"Michael"




when play begins:
	let start_rooms be { Pink room, Blue room, Yellow room, Lavender Room };
	sort start_rooms in random order; 
	repeat with the npc running through non-player-characters:
		choose a random row in the table of male names;
		now the printed name of the npc is name entry;
		now location is entry 1 of start_rooms;
		now the npc is in location;
		remove entry 1 from start_rooms;
		blank out the whole row;
	now location is entry 1 of start_rooms;
	now the smartphone is on the location;
	move the player to location, without printing a room description.


[TODO : NPC AI; Likes& dislikes ]

Part The Gaols

Chapter Garments and Uniforms

A uniform is a kind of thing.
A uniform has a list of garments called elements.

The Balconette bra is a bra with the printed name "[colour] lace balconette bra", unworn description "An [colour] lace underwired bra in a B cup, you imagine this might provide good support despite the fact it only covers the lower half of the breast leaving  a lot exposed. Suitable for wearing with a low cut dress.", and worn description "An [colour] lace underwired bra providing you with good support despite the fact it only covers the lower half of the breast leaving a lot exposed.". [TODO : check fit in wrn description ]
The size of the balconette bra is B. 

[xx]
the balconette bra is in the pink room.The colour of the balconette bra is "pink".


The push up bra is a bra with the printed name "[colour] push up bra", unworn description "A [colour] bra with full cups cut at an angle and trimmed with lace." and worn description "A [colour] bra with full cups, which are cut away towards the centre , creating a good cleavage."  [TODO : check fit in wrn description ]The size of the push up  bra is D.   


The corset is a bra with printed name "[colour] corset", unworn description "a heavy corset with a lot steel boning, neatly rolled in an unassuming tube.", worn description "a [colour] corset, which appears to be under tension compressing the wearers waist creating a lovely hourglass figure.".
.the cover areas  of a corset are usually {  upper torso area, upper back area, lower torso area, lower back area }.  [TODO : check fit in wrn description ] The size of the corset is C.   


A strapless bra is a bra with the printed name "[colour] strapless bra", and description "A [colour] strapless bra with a wide band. The underneath of which sports some lace embellishment in black.".
[TODO : check fit in wrn description ] The size of the strapless bra is C.  



The embellished corset is a bra with printed name "[colour] corset covered in sequins", unworn description "a heavy corset with a lot steel boning, cover with sequins depicted the logo of company.", worn description "a [colour] corset, which appears to be under tension compressing the wearers waist creating a lovely hourglass figure, the sequins giving the wearer an a look that can only be for the stage.".
.the cover areas of the embellished corset are usually {  upper torso area, upper back area, lower torso area, lower back area }.  [TODO : check fit in wrn description ] The size of the embellished corset is E.   


Frilly knickers are panties with the printed name "[colour] frilly knickers", unworn description "a pair of overtly feminine briefs with a many rows of ruffles sewn in giving the entire garment a frilly nature", worn description "a pair of overtly feminine briefs with a many rows of ruffles forming frills which 
capture the eye as the wear moves drawing your attention to their crotch". Frilly knickers are plural-named.

Floral knickers are panties with the printed name "[colour] knickers", description "a pair of overtly feminine [colour] briefs with a floral design.".

Regulation knickers are panties with the printed name "blue briefs", description "a pair of simple blue briefs.".
understand "briefs" as regulation knickers.

the Chastity device is panties with the printed name "steel chastity belt", unworn description "A steel chastity device which would force its wearer to have a smooth front crotch and to endure wearing the built-in wear cbutt plug", worn description " a steel chastity device completely hiding even the profile of the wearers sex".   
the chastity device is singular-named. The chastity device is not ambiguously plural. The chastity device is not rippable. The chastity device is not shiftable. The chastity device is block touching through.

[ Any gold necklace can only be removed by trying to wear another ]
Instead of taking off the chastity device:
	say "There doesn't seem to be anyway of removing this once it worn. It a chastity device, what did you expect.".
	
	

the short maid dress is a minidress with  printed name "black french maid dress" and description "A classic black A-line dress sporting a low neckline with white silk trim for accent and matching cuffs to set off the short puff sleeves. The rather short skirt of the dress is supported by an extremely bouffant petticoat.".
The shiftyness of a short maid dress is normally raisable.


the hobble maid dress is a dress with  printed name "black hobble maid dress" and description "An ankle length Black latex dress with a very straight and tight skirt. It sports long sleeves and high round collar. The collar is high enough to interfere with the wearers range of head movement, and as an added detail the collars and cuffs are accented with band of contrasting pink latex.".
a hobble maid dress is not shiftable. a hobble maid dress is normalwear.

the wedding dress is a minidress with  printed name "short wedding dress" and description "A knee length white strapless dress of a shiny material (probably silk) and bouffant skirt. The dress is embellished with just enough sequins to send out a bridal vibe."  

The tutu is a minidress with printed name "pink leotard with a sewn in tutu", and description "A classic ballerina tutu." 

The headdress is hat with the printed name "showgirl headdress", and description "A [colour] headdress with a fan of feathers reaching up to ceiling"  

The tartan skirt is a skirt with printed name "pleated tartan skirt", and description "Apart form the rather short length of this skirt, it bears a striking resemblance to the girls uniform at your old school."

The blouse is a shirt with printed name "simple white blouse", and description "A simple white blouse with just enough lace at the collar to give an undeniable feminine charm. Some of the top buttons are missing which  
mean it can't hide the bosoms of anybody wearing it."

The tiara is a hat with printed name "a diamonte bridal tiara", and description "A diamonte studded headdress, which might be intended to give the wear the illusion of royalty, but this one entirely fails to achieve this aim and just looks cheap." 

understand the printed name property as describing a garment.


check going while the player is wearing the hobble maid dress:
	if the player is kneeling:
		say "You can't crawl in such a tight skirt; you'll have to stand up first";
		stop the action.
		
after going while the player is wearing the hobble maid dress:
	say "The tight skirt forces you to take small steps ; each of which reminds you of the strange clothes you are wearing.";
	continue the action.


after going while the player is wearing a minidress:
	if the player is not wearing the tutu:[Tutu has it's own rule]
		say "The hem of the dress caresses your thighs as you walk, reminding you of the shortness of the skirt, and accentuating a unfamiliar sense of accessibility and vulnerability.";
		continue the action.

after going while the player is wearing a tutu:
	say "Your wide flat skirt not only makes it difficult to get through doorways, it seems to have a life of it's own. With every step you take it bounces, tugging at the leotard in different and unfamiliar ways.";
	continue the action.
 

When play begins:
	let pos_colours be { "pink", "blue", "yellow", "lavender", "white" }; 
	repeat with the cloth running through the garments:
		let destination be a random wardrobe;
		sort pos_colours in random order;
		[Only set the colour if unset as this order of execution of this
		 and the section below which sets up the necklaces' is undefined]
		if the colour of the cloth is "":
			now the colour of the cloth is entry  1 of pos_colours;
		now the cloth is in the destination.

School girl is a uniform. The elements of school girl are { balconette bra, regulation knickers, tartan skirt, blouse }.
French Maid is a uniform.  The elements of french maid are { push up bra, frilly knickers, short maid dress }.
Bondage Maid is a uniform. the elements of bondage maid are { corset, chastity device, hobble maid dress } .
Slutty bride is a uniform. the elements of slutty bride are { strapless bra,floral knickers, wedding dress, tiara }.
Ballerina is a uniform. the elements of ballerina are { tutu }.
show girl is a uniform. the elements of show girl are { embellished corset, headdress }. 





Chapter Sissy definitions

[Needs outfit, and cup size, hair style and max penis size entries ]
Table of sissy archetypes
Archetype		hair style		target cup size	uniform
"School Girl"	pigtails		B			School girl
"French Maid"	ringlets		D			French Maid
"Bondage Maid"	ponytail		C			Bondage Maid
"Slutty Bride"	curls			C			Slutty Bride
"Ballerina"		bun			AA			Ballerina
"Show girl"		--			E			show girl


Table of sissy names
Name
"Cindy"
"Alice"
"Missy"
"Suzette"
"Camille"
"Chloe"
"Daisy"
"Elise"
"Giselle"
"Lucie"
"Sophie"
"Marie"
"Cerise"
"Clarisse"
"Cosette"
"Melissa"
"Roselle"
"Fifi"
"Janice"




Part The Facility

Chapter the Map

Corner room is a kind of room. A Corner room has a text called colour.
The description of a corner room is "A Large room primarily painted in [colour] and all the fixtures
and fittings seem to match this colour. There is a [colour] wardrobe in one corner. In another two unusual tubes seem to hang from the ceiling just out of reach. Along one side is a dressing table with a large mirror. Finally and most oddly just above floor level a strange intrusion into the room which almost looks like a dildo, above which appears to be a screen.
".
Every a corner room contains a wardrobe.  a wardrobe is scenery.
[Dressing tables get created when we put a necklace on them] A dressing table is scenery.
Every corner rooms contains  wall-dildo. A wall-dildo is kind of sex toy. A wall-dildo is scenery.
Understand "intrusion" as wall-dildo. Understand "dildo" as wall-dildo.
Every corner rooms contains a screen. A screen is scenery.


[TODO : Add scenery for unusual tubes. Add nipple examination station to description ettc. ]

The decency threshold of a corner room is indecent.

The Pink room is a Corner room. The colour of the Pink room is "pink".
The Blue room is a Corner room. The colour of the Blue room is "baby blue".
The Yellow Room is a Corner room. The colour of the Yellow room is "yellow".
The Lavender Room is a Corner room. The colour of the lavender room is "lavender".

The Blue room is north of the pink room.
The Yellow room is north of the lavender room.
The Lavender room is east of the pink room.
The Yellow room is east of the blue room. 

The wayout is a room.

An exit is a kind of door. the indefinite article of an exit is "the".
 
The Pink Exit is northeast of the pink room and west of the wayout. The pink exit is a exit.
The Blue Exit is southeast of the blue room and north of the wayout. The Blue Exit is a exit.
The Yellow Exit is southwest of the yellow room and south of the wayout.The Yellow Exit is a exit.
The Lavender Exit is northwest of the lavender room and east of the wayout.The Lavender Exit is a exit.


Before an actor opening an exit:
	[ First report on the photographic bit ]
	if the actor is the player:
		say "As you press the button to exit, a bright light illuminates and momentarily blinds you.[line break][line break]After you blink away the after images in your eyes you can read a small display
	   above the button showing '[special-style-2]Processing...[roman type]' with a rotating symbol.";
	else:
		if the location of the player is the location of the actor:
			say "Your attention is drawn to the  [noun] by a bright flash, and you can [actor] standing waiting for the door to open";
			
Check someone opening an exit:
	[ continue if appropriate ]
	if the actor is dressed incorrectly: 
		stop the action.

Check opening an exit:
	[ continue if appropriate ]
	if the player is dressed incorrectly:
		display why the player is incorrectly dressed; 
		stop the action.


Report someone opening an exit:
	[The door did open]
	say "[The noun] slides opens and [person asked] steps through out of this nightmare into whatever awaits outside. The door slides closed behind before anyone else can pass through.";
	now the noun is closed.


Report opening an exit:
	say "[The noun] slides opens and you step through out of this nightmare into whatever awaits outside. The door slides closed behind before anyone else can pass through.";
	end the story.
	


Chapter The smartphone

A smartphone is a thing. The description is "A somewhat snazzy smartphone. Last year's model - all
you can afford. But has Dave's Mail notes so far on it."

Chapter Necklaces

A gold necklace is a kind of thing. A gold necklace is wearable. A gold necklace has some text called visible name. A gold necklace has some text called archetype. A gold necklace has a uniform called the workwear.

The description of a gold necklace is "A cheap looking gold coloured necklace, which is made from the name [visible name] cast in a script font with a matching gold chain attached to each end."

There is a gold necklace on the dressing table in the pink room.
There is a gold necklace on the dressing table in the blue room.
There is a gold necklace on the dressing table in the yellow room.
There is a gold necklace on the dressing table in the lavender room.

When play begins:
	[[ Setup each necklace with a random name and archetype ]]
	repeat with the necklace running through the gold necklaces:
		choose a random row in the table of sissy names;
		now the visible name of the necklace is name entry;
		blank out the whole row;
		choose a random row in the table of sissy archetypes;
		now the archetype of the necklace is the archetype entry;
		now the workwear of the necklace is the uniform entry;
		repeat with the cloth running through the elements of the uniform entry:
			[ Some things will take on the colour of the room where the necklace
			  for their wear is ]
			now the colour of the cloth is the colour of the location of the necklace. 
		[blank out the whole row.][ We can't blank out he whole row if we want to use the table to look up attributes; but then we might get duplicates. But that just more Fun.]


After examining a gold necklace:
	now the printed name of the noun is "[visible name] necklace";
	now the indefinite article of the noun is "the";
	continue the action.

understand the printed name property as describing a gold necklace.

[ Any gold necklace can only be removed by trying to wear another ]
Instead of taking off a gold necklace:
	say "It seems to be strangely attached. Nothing you can do seems to affect the clasp.".
	
	
After wearing a gold necklace:[ called the new necklace:]
	let new necklace be the noun;
	repeat with p running through person wearing the new necklace:
		repeat with n running through gold necklaces worn by p:
			if n is not the new necklace:
				say "As the clasp of [the new necklace] closes, then [the n] unclasps and falls straight to the floor.";
				now n is in the location of p.




[[TODO These two sub routines need to be merged for DRY ]]
to decide if (someone - a person) is dressed incorrectly:
	if someone is not wearing a gold necklace:
		decide yes;
	if the genitalia of someone is visible:
		if the psize of the genitalia of someone is greater than Inny:
			decide yes;
	let defining necklace be a gold necklace;
	repeat with x running through gold necklaces worn by someone:
		now defining necklace is x;
	repeat with garment running through the elements of workwear of the defining necklace:
		if someone is not wearing garment:
			decide yes;
	decide no;
	

to display why (someone - a person) is incorrectly dressed:
	if someone is not wearing a gold necklace:
		say "The door's display finally changes.. '[special-style-1]Access Denied: No necklace detected[roman type]'";
		stop;
	let defining necklace be a gold necklace;
	repeat with x running through gold necklaces worn by someone:
		now defining necklace is x;
	repeat with garment running through the elements of workwear of the defining necklace:
		if someone is not wearing garment:
			say "The door's display finally changes.. '[special-style-1]Access Denied: [Garment] not detected.[roman type]'";
			stop;
	if the genitalia of someone is visible:
		if the psize of the genitalia of someone is greater than Inny:
			say "The door's display finally changes.. '[special-style-1]Access Denied: Penis visible.[roman type]'";
			stop;
	

Chapter The Pills

[
Table of Pill Effects
Effect			type	body part	attribute 	amount
"Gives +1 breast size"	"b"	breasts
"Gives +2 breast size"	"b"	breasts
"Gives +3 breast size"	"b"	breasts
"Gives -1 breast size"	"b"	breasts
"Gives -1 penis size"	"p"
"Gives -2 penis size"	"p"
"Gives +1 penis size"	"p"
"Gives a vagina"		"p"
"Gives +1 arousal"	"x"
"Gives +1 SAN -1 DGN"	"x"
"Gives very long hair"	"x"
"Gives long hair" 	"x"
]

A pill bottle is a kind of container with description "a small brown glass bottle of the sort which normally contains pills". On every dressing table is a pill bottle. 


A pill is a kind of thing. a pill is edible. A pill has some text called colour. A pill has some text called shape. 
the printed name of a pill is "[shape] [colour] pill".
the printed plural name of a pill is "[shape] [colour] pills".
The medicine cabinet is a container. The medicine cabinet is in out-of-play.

understand the printed name property as describing a pill.


to inflate (victim - person) breasts by (amt - number) steps/step:
	let bosoms be the breasts of the victim;
	if amt is less than 0:
		repeat with counter running from -1 to amt:
			if the size of the bosoms is flat:
				break;
			now the size of bosoms is the cup size before the size of the bosoms;
		if the victim is the player:
			say "You feel your chest deflate, [if bosoms are visible]looking 
			     down you now see you have [size of bosoms] cup breasts.[end if]";
		else if victim is visible:
			say "you see [victim]'s chest deflate";
	else: 
		repeat with counter running from 1 to amt:
			if the size of the bosoms is E:
				break;
			now the size of bosoms is the cup size after the size of the bosoms;
		if the victim is the player:
			say "You feel your chest swell, [if bosoms are visible]looking 
			     down you now see you have [size of bosoms] cup breasts.[end if]";
		else if victim is visible:
			say "you see [victim]'s chest swell.";


to shrink (victim - person) dick by (amt - number) steps/step:
	[action]
	let dick be the genitalia of the victim;
	let original size be the psize of the dick;
	if amt is less than 0:
		repeat with counter running from -1 to amt:
			if the psize of the dick is Extra Large:
				break;
			now the psize of dick is the penis size before the psize of the dick;
	else: 
		repeat with counter running from 1 to amt:
			if the psize of the dick is Inny:
				break;
			now the psize of dick is the penis size after the psize of the dick;
	[Update f**king properties]
	if the psize of the dick is Inny:
		now the genitalia of the victim is orificial;
		now the genitalia of the victim is not penetrating;
	otherwise:
	 	now the genitalia of the victim is not orificial;
		now the genitalia of the victim is penetrating;
	[report; needs to be done after update.]
	if original size is not the psize of the dick:
		if amt is less than 0:
			if the victim is the player:
				say "You feel your dick grow, [if dick is visible]looking 
				     down you now see you have  [dick][end if]";
			else if victim is visible:
				say "you see [victim]'s penis grow";
		else: 
			if the victim is the player:
				say "You feel your dick shrink[no line break]";
				if psize of dick is Inny:
					say " and twist, and then feel your balls getting sucked inside your body. Leaving you with an uncomfortable empty feeling[no line break]";
				say ". You [if dick are visible]look 
				     down and now see you now have a [dick][end if][no line break]";
				if psize of dick is Inny:
					say ". Shit! This can't be real! Can it?";
			else if victim is visible:
				say "you see [victim]'s penis shrink.".



A type1_breast pill is a kind of pill.
the printed plural name of a type1_breast pill is "[shape] [colour] pills".
The medicine cabinet contains two type1_breast pills.
After an actor eating a type1_breast pill:
	inflate the actor breasts by 1 step. 
	[now the size of the actors breasts is increased by 1;]

A type2_breast pill is a kind of pill.
The medicine cabinet contains two type2_breast pills.
the printed plural name of a type2_breast pill is "[shape] [colour] pills".
After an actor eating a type2_breast pill:
	inflate the actor breasts by 2 steps.
	

A type3_breast pill is a kind of pill.
The medicine cabinet contains two type3_breast pills.
the printed plural name of a type3_breast pill is "[shape] [colour] pills".
After an actor eating a type3_breast pill:
	inflate the actor breasts by 3 steps.
	
A type4_breast pill is a kind of pill.
The medicine cabinet contains two type4_breast pills.
the printed plural name of a type4_breast pill is "[shape] [colour] pills".
After an actor eating a type4_breast pill:
	inflate the actor breasts by -1 steps.

A type1_penis pill is a kind of pill.
the printed plural name of a type1_penis pill is "[shape] [colour] pills".
The medicine cabinet contains two type1_penis pills.
After an actor eating a type1_penis pill:
	shrink the actor dick by 1 step.
	[let dick be the genitalia of actor;
	now the psize of dick is the penis size before the psize of the dick. 
	[no the psize of the actors genitalia is decreased by 1;]
]

A type2_penis pill is a kind of pill.
The medicine cabinet contains two type2_penis pills.
the printed plural name of a type2_penis pill is "[shape] [colour] pills".
After an actor eating a type2_penis pill:
	shrink the actor dick by 2 steps.
[
	let dick be the genitalia of actor;
	now the psize of dick is the penis size before the psize of the dick;
	now the psize of dick is the penis size before the psize of the dick. 
	[now the psize of the actors genitalia is decreased by 1;]
]
A type3_penis pill is a kind of pill.
The medicine cabinet contains two type3_penis pills.
the printed plural name of a type3_penis pill is "[shape] [colour] pills".
After an actor eating a type3_penis pill:
	shrink the actor dick by 3 steps.[
	let dick be the genitalia of actor;
	now the psize of dick is the penis size before the psize of the dick;
	now the psize of dick is the penis size before the psize of the dick;
	now the psize of dick is the penis size before the psize of the dick. 
	[now the psize of the actors genitalia is decreased by 1;]
]
A type4_penis pill is a kind of pill.
The medicine cabinet contains two type4_penis pills.
the printed plural name of a type4_penis pill is "[shape] [colour] pills".
After an actor eating a type4_penis pill:
	shrink the actor dick by -1 steps.
	[let dick be the genitalia of actor;
	now the psize of dick is the penis size after the psize of the dick. 
	[now the psize of the actors genit      alia is decreased by 1;]
]

when play begins:
	let pill_colours be { "blue", "pink", "lavender", "green" };
	let pill_shapes be { "round", "square", "oval", "diamond" };
	sort pill_colours in random order;
	sort pill_shapes in random order;
	[[repeat this for each type of pill we have. ]]	
	repeat with p running through the type1_breast pills:
		now the colour of p is (entry 1 of pill_colours);
		now the shape of p is (entry 1 of pill_shapes);
		now p is in a random pill bottle;
		[Randomly place the pill in the game]
	repeat with p running through the type2_breast pills:
		now the colour of p is (entry 2 of pill_colours);
		now the shape of p is (entry 1 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle;
	repeat with p running through the type3_breast pills:
		now the colour of p is (entry 3 of pill_colours);
		now the shape of p is (entry 1 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle;
	repeat with p running through the type4_breast pills:
		now the colour of p is (entry 4 of pill_colours);
		now the shape of p is (entry 1 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle;
	repeat with p running through the type1_penis pills:
		now the colour of p is (entry 1 of pill_colours);
		now the shape of p is (entry 2 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle;
	repeat with p running through the type2_penis pills:
		now the colour of p is (entry 2 of pill_colours);
		now the shape of p is (entry 2 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle;
	repeat with p running through the type3_penis pills:
		now the colour of p is (entry 3 of pill_colours);
		now the shape of p is (entry 2 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle;
	repeat with p running through the type4_penis pills:
		now the colour of p is (entry 4 of pill_colours);
		now the shape of p is (entry 2 of pill_shapes);
		[Randomly place the pill in the game]
		now p is in a random pill bottle.

[
The pink pill is in the pink room.
The blue pill is in the blue room.
The yellow pill is in the yellow room.
The lavender pill is in the lavender room.
]

Part The Story

Chapter the beginning

when play begins:
	say "Your live in a small town, which is miles from anywhere, and hope one day to get a job in the big city, but until then you still live with you parents and the only available job was as a cashier in the local supermarket. You took it of course.[line break][line break]
     A few months ago, a new development rocked the town to it's core. [line break][line break] 
	You see somebody somewhere decided your home town was the ideal place to open a new strip and Lap dance club called the 'House of Fun'.
	[line break]
	The grand opening was three weeks ago. It the run up there was all sorts of hand-wringing by the upstanding
	local politicians about decadence and decline of moral fibre that such an establishment
	would of course bring to your small community.
	[line break]	   
	You and your friends tried to get tickets, but they just didn't seem to be available. At all. Nobody you
	knew had any. Then even more weirdly you realised nobody you knew had a job there. You didn't notice straight
	away, most of your mates being blokes, but then you slowly realised none of the girls you spoke to worked  there or knew of anybody who worked there.
 	[line break][line break]
	Since the grand opening you know few people who have been able to scrap up the extortionate $200 entry 
	price. And they seeming to keeping quiet. Well quiet about the details, they all say the place is full of truly amazing looking girls and every thing else is limited only by your imagination. But won't be drawn further.";
	say " 
	[line break] The politicians have stopped complaining and the local bars are busy with new comers to town
	having a few drinks, before they enter the 'Lion's Den' as they often call the new venture. 
	[line break] Normally you would just ignore this, after all it's none of your business, it a good story to tell when you finally get to the big city, more and more people were moving out it was only a matter of time before you got that big job. Or so you thought. But at the beginning of the week, on of your regular
	   drinking buddies - Dave, started to have a bit of rant about the whole thing, it had clearly been eating
	away at him for sometime. Dave it turned out had been trying to keep in touch with some of those who
	had moved into the big city. But he had a list of names of those who he couldn't track down in the city, he
	wasn't surprised about that. But he claimed that greater proportion of those unfindables, had visited the
	'House of Fun' than those he could find. He claimed it was suspicious.[line break]
	You told Dave he was imagining it and bought the next round, an you and your mates all had a laugh at 
	Dave's expense. He promised to was going to prove it to you, he knew a secret way in to place and
	was going to break in come back with some evidence.  
	[line break]	
	You haven't seen Dave since.";
	say "
	[line break]
	Then last night, you, [npc1], [npc2] and [npc3] got an automatic email from Dave's account which 
	was timed to go off if he didn't return from his expedition. It contained his secret way into the Fun house,
	and what he knew so far. Which was very little. Somehow you all decided you need to rescue Dave. Which
	was when it all went wrong.[line break]
	Dave's secret way in was a plumbing maintenance area which happened to be just where the building was
	nearest the boundary fence. It was a simple job to cut through the fence and and gain access to the
	maintenance area. Unfortunately this seem to be set up like some sort of trap.[line break]
	You entered the area and there was clearly a leak in the pipes, as you slowly made you way down the entry corridor you clothes and hair begin to get soaked in water leaking from the celling. It didn't take long for your scalps to start itching. when you starting scratching you hair started to come out, and your clothes started itch against your skin too.
     Before you realised what you where doing you have all stripped down. Suddenly the pipe burst fully, or you think that was what happened. Either a great flood of water swept you along the corridor, until you got to grating at the far end through which the water drained. Leaving you naked and hairless, everything you brought with you seems to have been washed away except for the smart phone you had in your hand as the flood started.
	[line break]
	  Then as the last of the water disappeared, a trapdoor opened up and you each spun away down a series
	of chutes, ending up where you are now.[line break][line break]
	".




Chapter Happens when you lick the dildo

check an actor licking a wall-dildo:
	if the actor is standing:
		if the actor is the player:
			say "You need to kneel down to get that in your mouth.";
		stop the action.

report an actor licking a wall-dildo for the first time:
	say "Somewhat dubiously you crawl into position and place the item into your mouth,
	trying not to think about what it looks like.[line break]
	As you get yourself aligned you notice your knees and hands suddenly feel stuck in depressions
	on the floor. Odd.
	".


carry out an actor licking a wall-dildo:
	[ The first person to lick a dildo claim's it as theirs.
	Which makes a nice denial of resource game strategy possible]
	if the user of the noun is not-a-person:
		now the user of the noun is the actor;
	if the user of the noun is not the actor:
		let the dick be the genitalia of the actor;
		now the psize of dick is the penis size after the psize of the dick.
	

report an actor licking a wall-dildo while the actor is not wearing a gold necklace:
	say "You wrap you lips around the 'analyser', your eyes fixed on the screen which is so 
	close there is nowhere to look anyway. When the device is fully inside your mouth 
	the screen changes.[line break]";
	if the user of the noun is the actor:
		say "The screen now reads:[line break][special-style-2] To leave this facility, you must be clothed in your uniform,
		and have no penis showing. Please select and wear a necklace. Then return here for your uniform details[roman type]";
	otherwise:
		say "The screen flashes Red with [special-style-1]'UNAUTHORISED USER[line break]Applying penalty.'[roman type]";
		say " You feel the analyser squirt something into you mouth, you've 
		swallowed it before you can properly react, but that doesn't stop the foul
		taste invading your senses.";
	say " Once you have removed yourself from the devices clutches, the screen returns 
	to it's previous message".
		  
	

report an actor licking a wall-dildo while the actor is wearing a gold necklace:
	say "You wrap you lips around the 'analyser', your eyes fixed on the screen which is so 
	close there is nowhere to look anyway. When the device is fully inside your mouth 
	the screen changes.[line break]";
	if the user of the noun is the actor:
		say "The screen now reads:[line break] [special-style-2]To leave this facility, you must be clothed in your uniform,
		and have no penis showing.";
		repeat with gn running through the gold necklaces worn by the actor:
			say "You have been chosen as a [archetype of the gn][line break]";
		 	say "Your uniform is [the elements of the workwear of gn]";
		say "[roman type]";
	otherwise:
		say "The screen flashes Red with '[special-style-1]UNAUTHORISED USER[line break]Applying penalty.'[roman type]";
		say "You feel the analyser squirt something into you mouth, you've 
		swallowed it before you can properly react, but that doesn't stop the foul
		taste invading your senses.";
	say "[line break]";
	say "Once you remove yourself from the devices clutches the screen returns 
	to it's previous message".
		  

check an actor fucking a wall-dildo:
	if the actor is standing:
		if the actor is the player:
			say "You need to kneel down to reach that.";
		stop the action.


Chapter The End

the final epilogue rule is listed instead of the print obituary headline rule in for printing the player's obituary.
This is the final epilogue rule:
	let defining necklace be a gold necklace;
	repeat with x running through gold necklaces worn by someone:
		now defining necklace is x; 
 	say "FINAL EPILOGUE[line break][line break]In the final game this is where you find out how the rest of your life as [archetype of the defining necklace] whore goes, and whether you solved the mystery of what happened to Bill, and discovered any of the secrets of the House of Fun.". 

